#include "Arduino.h"
#include "encoder_driver.h"
#include "eeprom_driver.h"
#include "motion_driver.h"
#include "input_driver.h"
#include "indicator_driver.h"

#define EE_SAVED_ADDRESS1  0
#define EE_SAVED_ADDRESS2  8
#define EE_SAVED_ADDRESS3  16
#define EE_SAVED_ADDRESS4  24
#define EE_SAVED_ADDRESS5  32
#define EE_SAVED_ADDRESS6  40
#define EE_SAVED_ADDRESS7  48
#define EE_SAVED_ADDRESS8  56
#define EE_SAVED_ADDRESS9  64
#define EE_SAVED_ADDRESS10 72
#define EE_SAVED_ADDRESS11 80
#define EE_SAVED_ADDRESS12 88

uint32_t saved_E1; uint32_t saved_E2; uint32_t saved_E3;
uint32_t saved_E4; uint32_t saved_E5; uint32_t saved_E6;
uint32_t saved_E7; uint32_t saved_E8; uint32_t saved_E9;
uint32_t saved_E10; uint32_t saved_E11; uint32_t saved_E12;

int E1; int E2; int E3;
int E4; int E5; int E6;
int E7; int E8; int E9;
int E10; int E11; int E12;

int totalStroke = 27566;
int totalStrokeStored = 11111;
int totalStrokeNotStored = 11122;

void initEeprom(){
  saved_E1 = eeprom_read_dword((const uint32_t *)EE_SAVED_ADDRESS1);
  saved_E2 = eeprom_read_dword((const uint32_t *)EE_SAVED_ADDRESS2);
  saved_E3 = eeprom_read_dword((const uint32_t *)EE_SAVED_ADDRESS3);
  saved_E4 = eeprom_read_dword((const uint32_t *)EE_SAVED_ADDRESS4);
  saved_E5 = eeprom_read_dword((const uint32_t *)EE_SAVED_ADDRESS5);
  saved_E6 = eeprom_read_dword((const uint32_t *)EE_SAVED_ADDRESS6);
  saved_E7 = eeprom_read_dword((const uint32_t *)EE_SAVED_ADDRESS7);
  saved_E8 = eeprom_read_dword((const uint32_t *)EE_SAVED_ADDRESS8);
  saved_E9 = eeprom_read_dword((const uint32_t *)EE_SAVED_ADDRESS9);
  saved_E10 = eeprom_read_dword((const uint32_t *)EE_SAVED_ADDRESS10);
  saved_E11 = eeprom_read_dword((const uint32_t *)EE_SAVED_ADDRESS11);
  saved_E12 = eeprom_read_dword((const uint32_t *)EE_SAVED_ADDRESS12);
  E1 = saved_E1;
  E2 = saved_E2;
  E3 = saved_E3;
  E4 = saved_E4;
  E5 = saved_E5;
  E6 = saved_E6;
  E7 = saved_E7;
  E8 = saved_E8;
  E9 = saved_E9;
  E10 = saved_E10;
  E11 = saved_E11;
  E12 = saved_E12;
  if (E11 == totalStrokeStored){
   totalStroke = E12;
  }
}

int getStoredPosition(int Position){
  unsigned int returnValue;
  switch (Position) {
    case 1: returnValue = E1; break;
    case 2: returnValue = E2; break;
    case 3: returnValue = E3; break;
    case 4: returnValue = E4; break;
    case 5: returnValue = E5; break;
    case 6: returnValue = E6; break;
    case 7: returnValue = E7; break;
    case 8: returnValue = E8; break;
    case 9: returnValue = E9; break;
    case 10: returnValue = E10; break;
  }
  return returnValue;
}

int getTotalStroke(){
  unsigned int returnValue;
  returnValue = totalStroke;
  return returnValue;
}

void restoreTotalStroke(){
  E11 = totalStrokeNotStored;
  saved_E11 = totalStrokeNotStored;
  eeprom_write_dword((uint32_t *)EE_SAVED_ADDRESS11, saved_E11);
  totalStroke = 27566;
}

int storePosition(int Position){
  switch (Position) {
    case 1: E1 = getEncoderPosition(); saved_E1 = getEncoderPosition(); eeprom_write_dword((uint32_t *)EE_SAVED_ADDRESS1, saved_E1); break;
    case 2: E2 = getEncoderPosition(); saved_E2 = getEncoderPosition(); eeprom_write_dword((uint32_t *)EE_SAVED_ADDRESS2, saved_E2); break;
    case 3: E3 = getEncoderPosition(); saved_E3 = getEncoderPosition(); eeprom_write_dword((uint32_t *)EE_SAVED_ADDRESS3, saved_E3); break;
    case 4: E4 = getEncoderPosition(); saved_E4 = getEncoderPosition(); eeprom_write_dword((uint32_t *)EE_SAVED_ADDRESS4, saved_E4); break;
    case 5: E5 = getEncoderPosition(); saved_E5 = getEncoderPosition(); eeprom_write_dword((uint32_t *)EE_SAVED_ADDRESS5, saved_E5); break;
    case 6: E6 = getEncoderPosition(); saved_E6 = getEncoderPosition(); eeprom_write_dword((uint32_t *)EE_SAVED_ADDRESS6, saved_E6); break;
    case 7: E7 = getEncoderPosition(); saved_E7 = getEncoderPosition(); eeprom_write_dword((uint32_t *)EE_SAVED_ADDRESS7, saved_E7); break;
    case 8: E8 = getEncoderPosition(); saved_E8 = getEncoderPosition(); eeprom_write_dword((uint32_t *)EE_SAVED_ADDRESS8, saved_E8); break;
    case 9: E9 = getEncoderPosition(); saved_E9 = getEncoderPosition(); eeprom_write_dword((uint32_t *)EE_SAVED_ADDRESS9, saved_E9); break;
    case 10: E10 = getEncoderPosition(); saved_E10 = getEncoderPosition(); eeprom_write_dword((uint32_t *)EE_SAVED_ADDRESS10, saved_E10); break;
  }
}

void totalStrokeCalibrateHandler(){
  unsigned int i;
  for (i = 0; i < 3; i++) {
    ledBlink(2000);
    if (getUpButtonState() == true) {
      break;
    }
    if (getInputState(1) == true) {
      break;
    }
  }
  if (i > 2) {
    for (int i = 0; i < 10; i++) {
      ledBlink(50);
    }
    E11 = totalStrokeStored;
    saved_E11 = totalStrokeStored;
    eeprom_write_dword((uint32_t *)EE_SAVED_ADDRESS11, saved_E11);
    getEncoderPosition();
    E12 = getEncoderPosition();
    saved_E12 = getEncoderPosition();
    eeprom_write_dword((uint32_t *)EE_SAVED_ADDRESS12, saved_E12);
    totalStroke = E12;
  }
  else {
    ledOff();
  }
  ledOff();
}

void positionCalibrateHandler(int Position){
  unsigned int i;
  for (i = 0; i < 3; i++) {
    ledBlink(500);
    if (getInputState(Position) == true) {
      break;
    }
  }
  if (i > 2) {
    for (int i = 0; i < 2; i++) {
      ledBlink(100);
    }
    storePosition(Position);
  }
  else {
    ledOff();
  }
  ledOff();
}
