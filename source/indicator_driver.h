#ifndef __INDICATOR_DRIVER_H
#define __INDICATOR_DRIVER_H

void ledOn();
void ledOff();
void ledBlink(int blinkPace);
void ledRestoreStroke();

#endif
