#ifndef __EEPROM_DRIVER_H
#define __EEPROM_DRIVER_H

void initEeprom();
int getStoredPosition(int Position);
int getTotalStroke();
void restoreTotalStroke();
int storePosition(int Position);
void totalStrokeCalibrateHandler();
void positionCalibrateHandler(int Position);

#endif
