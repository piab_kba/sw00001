#include "Arduino.h"
#include "encoder_driver.h"
#include "eeprom_driver.h"
#include "motion_driver.h"
#include "input_driver.h"
#include "indicator_driver.h"

void ledOn(){
  digitalWrite(13, HIGH);
}

void ledOff(){
  digitalWrite(13, LOW);
}

void ledBlink(int blinkPace){
  ledOn();
  delay(blinkPace);
  ledOff();
  delay(blinkPace);
}

void ledRestoreStroke(){
  for (int i = 0; i < 20; i++){
    ledBlink(50);
  }
}
