#include "Arduino.h"
#include "encoder_driver.h"
#include "eeprom_driver.h"
#include "motion_driver.h"
#include "input_driver.h"
#include "indicator_driver.h"

bool getKeyState(){
  bool returnState;
  returnState = digitalRead(12);
  return returnState;
}

bool getLimitSwitchState(){
  bool returnState;
  returnState = digitalRead(7);
  return returnState;
}

bool getFastSlowState(){
  bool returnState;
  returnState = digitalRead(5);
  return returnState;
}

bool getUpButtonState(){
  bool returnState;
  returnState = digitalRead(4);
  return returnState;
}

bool getDownButtonState(){
  bool returnState;
  returnState = digitalRead(6);
  return returnState;
}

bool getInputState(int inputNumber){
  bool returnState;
  unsigned int readInput;
  switch (inputNumber) {
    case 1: readInput = 14; break;
    case 2: readInput = 15; break;
    case 3: readInput = 16; break;
    case 4: readInput = 17; break;
    case 5: readInput = 18; break;
    case 6: readInput = 19; break;
    case 9: readInput = 9; break;
    case 10: readInput = 10; break;
    case 7: if (analogRead(A6) < 900){ returnState = false; } else{ returnState = true; } return returnState;
    case 8: if (analogRead(A7) < 900){ returnState = false; } else{ returnState = true; } return returnState;
  }
  returnState = digitalRead(readInput);
  return returnState;
}
