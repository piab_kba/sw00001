#ifndef __MOTION_DRIVER_H
#define __MOTION_DRIVER_H
#include <Adafruit_MCP4725.h>

void moveUp();
void moveDown();
void moveStop();
void moveFast();
void moveSlow();
void initMotionSystem();
void breakInProcess();
void calibrateEncoder();
int positionMove(int buttonNumber);
void upMove();
void downMove();

#endif
