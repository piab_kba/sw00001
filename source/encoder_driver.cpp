#include "Arduino.h"
#include "encoder_driver.h"
#include "eeprom_driver.h"
#include "motion_driver.h"
#include "input_driver.h"
#include "indicator_driver.h"

const byte Encoder0PinA = 2;
const byte Encoder0PinB = 3;

bool phaseA = false;
bool phaseB = false;
bool lastA = false;
bool lastB = false;

unsigned int Encoder0Pos;

void initEncoder(){
  attachInterrupt(digitalPinToInterrupt(Encoder0PinA), Encoder, CHANGE);
  //attachInterrupt(digitalPinToInterrupt(Encoder0PinB), Encoder, RISING);
  pinMode(Encoder0PinA, INPUT_PULLUP);
  pinMode(Encoder0PinB, INPUT_PULLUP);
  phaseA = digitalRead(Encoder0PinA);
  phaseB = digitalRead(Encoder0PinB);
}

int getEncoderPosition(){
  return Encoder0Pos;
}

int setEncoder(int value){
  Encoder0Pos = value;
}

void Encoder() {
  phaseA = digitalRead(Encoder0PinA);
  phaseB = digitalRead(Encoder0PinB);
  if (phaseA != lastA){
    if (phaseA == HIGH){
      if (phaseB != HIGH){
        Encoder0Pos = --Encoder0Pos;
      }
      else{
        Encoder0Pos = ++Encoder0Pos;
      }
    }
  }
  lastA = phaseA;
  lastB = phaseB;
}
