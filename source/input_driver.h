#ifndef __INPUT_DRIVER_H
#define __INPUT_DRIVER_H

bool getKeyState();
bool getLimitSwitchState();
bool getFastSlowState();
bool getUpButtonState();
bool getDownButtonState();
bool getInputState(int inputNumber);


#endif
